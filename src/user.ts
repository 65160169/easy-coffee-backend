import { AppDataSource } from "./data-source";
import { Role } from "./entity/Role";
import { User } from "./entity/User";

AppDataSource.initialize()
  .then(async () => {
    const userRepository = AppDataSource.getRepository(User);
    const rolesReposiory = AppDataSource.getRepository(Role);

    const adminRole = await rolesReposiory.findOneBy({ id: 1 });
    const userRole = await rolesReposiory.findOneBy({ id: 2 });

    console.log("Inserting a new user into the Memory...");
    var user = new User();
    user.id = 1;
    user.email = "admin@email.com";
    user.password = "Pass@1234";
    user.gender = "male";
    user.roles = [];
    user.roles.push(adminRole);
    user.roles.push(userRole);
    console.log("Inserting a new user into the Database...");
    await userRepository.save(user);

    //user1
    console.log("Inserting a new user into the Memory...");
    user = new User();
    user.id = 2;
    user.email = "user1@email.com";
    user.password = "Pass@1234";
    user.gender = "male";
    user.roles = [];
    user.roles.push(userRole);
    console.log("Inserting a new user into the Database...");
    await userRepository.save(user);

    //User2
    console.log("Inserting a new user into the Memory...");
    user = new User();
    user.id = 3;
    user.email = "user2@email.com";
    user.password = "Pass@1234";
    user.gender = "female";
    user.roles = [];
    user.roles.push(userRole);
    console.log("Inserting a new user into the Database...");
    await userRepository.save(user);

    const users = await userRepository.find({ relations: { roles: true } });
    console.log(JSON.stringify(users, null, 2));

    const roles = await rolesReposiory.find({ relations: { users: true } });
    console.log(JSON.stringify(roles, null, 2));
  })
  .catch((error) => console.log(error));
